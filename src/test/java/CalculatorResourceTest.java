import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import resources.CalculatorResource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+1";
        assertEquals(401, calculatorResource.calculate(expression));

        expression = "300-99-1";
        assertEquals(200, calculatorResource.calculate(expression));

        String multiplicationExpression = "10*3*10";
        assertEquals(300, calculatorResource.multiplication(multiplicationExpression));

        String divisionExpression = "30/3/10";
        assertEquals(1, calculatorResource.division(divisionExpression));

        divisionExpression = "100/5/2/5";
        assertEquals(2, calculatorResource.division(divisionExpression));

    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }
}
